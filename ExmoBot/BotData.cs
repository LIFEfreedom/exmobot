﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExmoBot
{
	// Информация о новой паре.
	public struct TradeInfo
	{
		public string NamePair;			// Наименование пары.
		public float CanSpend;			// Количество у.е. для траты.
		public bool AllBalance;			// Торговать на весь балланс.
		public DateTime createDate;     // Дата создания ордера.
	}
}
