﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExmoBot
{
	struct UserInfo
	{
		public int UID { get; set; }
		public Dictionary<string, string> Balances { get; set; }
		public Dictionary<string, string> Reserved { get; set; }
	}

	struct Ticker
	{
		public float Buy_Price { get; set; }
		public float Sell_Price { get; set; }
		public float Last_Trade { get; set; }
		public float High { get; set; }
		public float Low { get; set; }
		public float Avg { get; set; }
		public float Vol { get; set; }
		public float Vol_curr { get; set; }
	}

	struct User_Open_Orders
	{
		public int Order_Id;
		public long Created;
		public float Amount;
		public float Quantity;
		public float Price;
		public string Type;
	}

	struct OrderCreateResult
	{
		public bool Result { get; set; }
		public string Error { get; set; }
		public int Order_ID { get; set; }
	}

	struct PairSetting
	{
		public float Min_Quantity { get; set; }
		public float Max_Quantity { get; set; }
		public float Min_Price { get; set; }
		public float Max_Price { get; set; }
		public float Min_Amount { get; set; }
		public float Max_Amount { get; set; }
	}
}
