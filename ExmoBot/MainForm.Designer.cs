﻿namespace ExmoBot
{
	partial class MainForm
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.newUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
			this.newTradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.statisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.volumeListBox = new System.Windows.Forms.ListBox();
			this.spredListBox = new System.Windows.Forms.ListBox();
			this.VolotyList = new System.Windows.Forms.ListBox();
			this.time = new System.Windows.Forms.Label();
			this.volume = new System.Windows.Forms.Label();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.countsTrade = new System.Windows.Forms.Label();
			this.updateTimer = new System.Windows.Forms.Timer(this.components);
			this.menuStrip1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem3,
            this.aboutToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(884, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newUserToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
			this.toolStripMenuItem1.Text = "File";
			// 
			// newUserToolStripMenuItem
			// 
			this.newUserToolStripMenuItem.Name = "newUserToolStripMenuItem";
			this.newUserToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
			this.newUserToolStripMenuItem.Text = "New User";
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Enabled = false;
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(133, 22);
			this.toolStripMenuItem2.Text = "Delete User";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(130, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTradeToolStripMenuItem,
            this.statisticsToolStripMenuItem});
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(48, 20);
			this.toolStripMenuItem3.Text = "Trade";
			// 
			// newTradeToolStripMenuItem
			// 
			this.newTradeToolStripMenuItem.Name = "newTradeToolStripMenuItem";
			this.newTradeToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
			this.newTradeToolStripMenuItem.Text = "New Trade";
			this.newTradeToolStripMenuItem.Click += new System.EventHandler(this.newTradeToolStripMenuItem_Click);
			// 
			// statisticsToolStripMenuItem
			// 
			this.statisticsToolStripMenuItem.Name = "statisticsToolStripMenuItem";
			this.statisticsToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
			this.statisticsToolStripMenuItem.Text = "Statistic";
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4});
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
			this.aboutToolStripMenuItem.Text = "About";
			// 
			// toolStripMenuItem4
			// 
			this.toolStripMenuItem4.Name = "toolStripMenuItem4";
			this.toolStripMenuItem4.Size = new System.Drawing.Size(152, 22);
			this.toolStripMenuItem4.Text = "?";
			this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Location = new System.Drawing.Point(0, 439);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(884, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.Text = "notifyIcon1";
			this.notifyIcon1.Visible = true;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.volumeListBox);
			this.tabPage1.Controls.Add(this.spredListBox);
			this.tabPage1.Controls.Add(this.VolotyList);
			this.tabPage1.Controls.Add(this.time);
			this.tabPage1.Controls.Add(this.volume);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(876, 389);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Statistics";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// volumeListBox
			// 
			this.volumeListBox.Dock = System.Windows.Forms.DockStyle.Right;
			this.volumeListBox.FormattingEnabled = true;
			this.volumeListBox.Location = new System.Drawing.Point(333, 3);
			this.volumeListBox.Name = "volumeListBox";
			this.volumeListBox.Size = new System.Drawing.Size(180, 383);
			this.volumeListBox.TabIndex = 6;
			// 
			// spredListBox
			// 
			this.spredListBox.Dock = System.Windows.Forms.DockStyle.Right;
			this.spredListBox.FormattingEnabled = true;
			this.spredListBox.Location = new System.Drawing.Point(513, 3);
			this.spredListBox.Name = "spredListBox";
			this.spredListBox.Size = new System.Drawing.Size(180, 383);
			this.spredListBox.TabIndex = 5;
			// 
			// VolotyList
			// 
			this.VolotyList.Dock = System.Windows.Forms.DockStyle.Right;
			this.VolotyList.FormattingEnabled = true;
			this.VolotyList.Location = new System.Drawing.Point(693, 3);
			this.VolotyList.Name = "VolotyList";
			this.VolotyList.Size = new System.Drawing.Size(180, 383);
			this.VolotyList.TabIndex = 4;
			// 
			// time
			// 
			this.time.AutoSize = true;
			this.time.Location = new System.Drawing.Point(8, 16);
			this.time.Name = "time";
			this.time.Size = new System.Drawing.Size(33, 13);
			this.time.TabIndex = 3;
			this.time.Text = "Time:";
			// 
			// volume
			// 
			this.volume.AutoSize = true;
			this.volume.Location = new System.Drawing.Point(8, 3);
			this.volume.Name = "volume";
			this.volume.Size = new System.Drawing.Size(42, 13);
			this.volume.TabIndex = 2;
			this.volume.Text = "Volume";
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 24);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(884, 415);
			this.tabControl1.TabIndex = 3;
			// 
			// countsTrade
			// 
			this.countsTrade.AutoSize = true;
			this.countsTrade.Location = new System.Drawing.Point(4, 350);
			this.countsTrade.Name = "countsTrade";
			this.countsTrade.Size = new System.Drawing.Size(68, 13);
			this.countsTrade.TabIndex = 4;
			this.countsTrade.Text = "CountsTrade";
			// 
			// updateTimer
			// 
			this.updateTimer.Enabled = true;
			this.updateTimer.Interval = 500;
			this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(884, 461);
			this.Controls.Add(this.countsTrade);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.menuStrip1);
			this.Name = "MainForm";
			this.Text = "ExmoBot";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem newUserToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
		private System.Windows.Forms.ToolStripMenuItem newTradeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.ToolStripMenuItem statisticsToolStripMenuItem;
		private System.Windows.Forms.Label volume;
		private System.Windows.Forms.Label countsTrade;
		private System.Windows.Forms.Timer updateTimer;
		private System.Windows.Forms.Label time;
		private System.Windows.Forms.ListBox VolotyList;
		private System.Windows.Forms.ListBox volumeListBox;
		private System.Windows.Forms.ListBox spredListBox;
	}
}

