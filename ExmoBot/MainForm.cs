﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace ExmoBot
{
	public partial class MainForm : Form
	{
		private UserInfo userInfo;

		private int tradeNumber;

		List<TradeInfo> Trades;             // Список текущих торгов.
		List<TradeInfo> OrdersSell;

		public static List<string> NamePairs = new List<string>();

		public MainForm()
		{
			InitializeComponent();

			Trades = new List<TradeInfo>();
		}
		
		private void ExitProgram( )
		{
			// Отменяем все сделки



			// Закрываем программу
			Application.Exit();
		}

		public void AddNewPair(TradeInfo tradeInfo)
		{
			Trades.Add(tradeInfo);

			var result = ExmoApi.ApiQuery("user_open_orders", new Dictionary<string, string>());

			IDictionary<string, IList<User_Open_Orders>> Orders = JsonConvert.DeserializeObject<IDictionary<string, IList<User_Open_Orders>>> (result);

			return;
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			ExmoApi.SetKeys("K-fddad448c76a03d50616b55d925d1e5a2be2f20d", "S-d698879e82e0ae9c8bbd2e6cfa676b1ec9c07f93");

			//sync query
			var result = ExmoApi.ApiQuery("user_info", new Dictionary<string, string>());

			userInfo = JsonConvert.DeserializeObject<UserInfo>(result);

			volume.Text = userInfo.UID.ToString();

			IDictionary<string, Ticker> Pairs;

			Pairs = JsonConvert.DeserializeObject<IDictionary<string, Ticker>>(ExmoApi.ApiQuery("ticker", new Dictionary<string, string>()));

			List<string> voloty = new List<string>();
			List<string> volumeArr = new List<string>();
			List<string> spred = new List<string>();

			NamePairs.Clear();

			foreach (var pair in Pairs)
			{
				voloty.Add(pair.Key + " : \t" + ((pair.Value.High / pair.Value.Low - 1) * 100) + "%");
				volumeArr.Add(pair.Key + " : \t" + Decimal.Round ((Decimal)pair.Value.Vol_curr, 12).ToString() + " " + pair.Key.Substring(pair.Key.IndexOf("_") + 1));
				spred.Add(pair.Key + " : \t" + ((pair.Value.Sell_Price / pair.Value.Buy_Price - 1) * 100) + "%");
				NamePairs.Add(pair.Key);
			}

			VolotyList.Items.AddRange(voloty.ToArray());

			volumeListBox.Items.AddRange(volumeArr.ToArray());

			spredListBox.Items.AddRange(spred.ToArray());
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			ExitProgram();
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ExitProgram();
		}

		private void newTradeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewTradeForm newTrade = new NewTradeForm();

			newTrade.ShowDialog(this);
		}

		private void updateTimer_Tick(object sender, EventArgs e)
		{
			updateTimer.Stop();
			time.Text = "Time: " + DateTime.Now.ToShortTimeString() + " прошло секунд";




			// Переход на следующую пару.
			++tradeNumber;

			// 
			if (tradeNumber >= Trades.Count)
			{
				tradeNumber = 0;
			}
			updateTimer.Start();
		}

		private void toolStripMenuItem4_Click(object sender, EventArgs e)
		{
			AboutBox about = new AboutBox();
			about.ShowDialog(this);
		}
	}
}
