﻿namespace ExmoBot
{
	partial class NewTradeForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.selectPairBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.balance = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cost = new System.Windows.Forms.TextBox();
			this.createTrade = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.minumumLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// selectPairBox
			// 
			this.selectPairBox.FormattingEnabled = true;
			this.selectPairBox.Location = new System.Drawing.Point(59, 10);
			this.selectPairBox.Name = "selectPairBox";
			this.selectPairBox.Size = new System.Drawing.Size(121, 21);
			this.selectPairBox.TabIndex = 0;
			this.selectPairBox.SelectedIndexChanged += new System.EventHandler(this.selectPairBox_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Select:";
			// 
			// balance
			// 
			this.balance.AutoSize = true;
			this.balance.Location = new System.Drawing.Point(186, 13);
			this.balance.Name = "balance";
			this.balance.Size = new System.Drawing.Size(46, 13);
			this.balance.TabIndex = 2;
			this.balance.Text = "Balance";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 44);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(31, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Cost:";
			// 
			// cost
			// 
			this.cost.Location = new System.Drawing.Point(59, 41);
			this.cost.MaxLength = 10;
			this.cost.Name = "cost";
			this.cost.Size = new System.Drawing.Size(121, 20);
			this.cost.TabIndex = 4;
			// 
			// createTrade
			// 
			this.createTrade.Location = new System.Drawing.Point(189, 73);
			this.createTrade.Name = "createTrade";
			this.createTrade.Size = new System.Drawing.Size(75, 23);
			this.createTrade.TabIndex = 5;
			this.createTrade.Text = "Create";
			this.createTrade.UseVisualStyleBackColor = true;
			this.createTrade.Click += new System.EventHandler(this.createTrade_Click);
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new System.Drawing.Point(189, 43);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(80, 17);
			this.checkBox1.TabIndex = 7;
			this.checkBox1.Text = "All ballance";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// minumumLabel
			// 
			this.minumumLabel.AutoSize = true;
			this.minumumLabel.Location = new System.Drawing.Point(13, 78);
			this.minumumLabel.Name = "minumumLabel";
			this.minumumLabel.Size = new System.Drawing.Size(48, 13);
			this.minumumLabel.TabIndex = 8;
			this.minumumLabel.Text = "Minimum";
			// 
			// NewTradeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 104);
			this.Controls.Add(this.minumumLabel);
			this.Controls.Add(this.checkBox1);
			this.Controls.Add(this.createTrade);
			this.Controls.Add(this.cost);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.balance);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.selectPairBox);
			this.Name = "NewTradeForm";
			this.Text = "Create new trade";
			this.Load += new System.EventHandler(this.NewTradeForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox selectPairBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label balance;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox cost;
		private System.Windows.Forms.Button createTrade;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Label minumumLabel;
	}
}