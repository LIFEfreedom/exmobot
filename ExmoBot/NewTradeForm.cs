﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace ExmoBot
{
	// Форма создания новой пары для торговли.
	public partial class NewTradeForm : Form
	{
		// Информация о пользователе.
		private UserInfo userInfo;

		public NewTradeForm()
		{
			// Создание компонентов формы.
			InitializeComponent();
		}

		// Событие клика чекбокса "All ballance"
		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			if (this.checkBox1.Checked == true)
			{
				this.cost.Enabled = false;
			}
			else
			{
				this.cost.Enabled = true;
			}
		}

		private void NewTradeForm_Load(object sender, EventArgs e)
		{
			selectPairBox.Items.AddRange( MainForm.NamePairs.ToArray());
			
			//sync query
			var result = ExmoApi.ApiQuery("user_info", new Dictionary<string, string>());

			userInfo = JsonConvert.DeserializeObject<UserInfo>(result);
		}

		private void selectPairBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			balance.Text = userInfo.Balances[ ((string)selectPairBox.SelectedItem).Substring(((string)selectPairBox.SelectedItem).IndexOf("_") + 1)];

			IDictionary<string, PairSetting> pairSetting;

			//sync query
			var result = ExmoApi.ApiQuery("pair_settings", new Dictionary<string, string>());

			pairSetting = JsonConvert.DeserializeObject<IDictionary<string, PairSetting>>(result);

			IDictionary<string, Ticker> Pairs;

			Pairs = JsonConvert.DeserializeObject<IDictionary<string, Ticker>>(ExmoApi.ApiQuery("ticker", new Dictionary<string, string>()));
			
			minumumLabel.Text = (Pairs[(string)selectPairBox.SelectedItem].Last_Trade * pairSetting[(string)selectPairBox.SelectedItem].Min_Quantity).ToString();
		}

		private void createTrade_Click(object sender, EventArgs e)
		{
			try
			{
				if (checkBox1.Enabled == true || cost.Text != "")
				{
					TradeInfo tradeInfo = new TradeInfo();

					tradeInfo.AllBalance = this.checkBox1.Checked;
					tradeInfo.NamePair = this.selectPairBox.SelectedItem.ToString();

					if (cost.Enabled == true)
					{
						tradeInfo.CanSpend = Int32.Parse(cost.Text);
					}
					else tradeInfo.CanSpend = 0;

					((MainForm)Owner).AddNewPair(tradeInfo);

					this.Close();
				}
				else
				{
					MessageBox.Show("Please, enter sum", "Error!");
				}
			}
			catch (FormatException ex)
			{
				MessageBox.Show("Please, enter right sum", "Error!");
			}
		}
	}
}
